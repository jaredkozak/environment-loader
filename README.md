# Environment Loader

Environment variable loading tool used to simplify loading environment variables in typescript. It provides a standardized, flattened format for environment variables which is then converted into a typescript typed object.

## Installation

`npm install @kozakatak/environment-loader`

When working in development, you will need to use something like [dotenv](https://www.npmjs.com/package/dotenv) to load environment values from a .env file.

## Usage

Create a new file, call it something like '_cfg.ts'.


`_cfg.ts`:
```
import { environment, UNDEFINED_STRING, required, requiredInProduction, UNDEFINED_NUMBER, UNDEFINED_BOOLEAN } from '@kozakatak/environment-loader';


export const cfg = environment({
    // An error will be thrown if this is not provided.
    email: required(),

    url_frontend: '/',

    production: false,
    address: '127.0.0.1',
    port: 1337,

    database: {
        address: '127.0.0.1',
        port: UNDEFINED_NUMBER,
        encrypted: UNDEFINED_BOOLEAN,
    }


    cookies: {
        // 'topSecret' is set as the default when NODE_ENV is not 'development', otherwise an error will be thrown
        secret: requiredInProduction('topSecret'),
        domain: UNDEFINED_STRING,
    },


}, {
    // Define aliases here (optional, without prefix)
    // Aliases will be used if the environment value is not.
    COOKIE_SECRET: 'cookies_secret',
}, 'el'); // <-- Set the prefix for environment variables here. Default is "app"

// You can then use config values like this:

console.log(`Connecting to database: ${ cfg.database.address }:${ cfg.database.port } ${ encrypted ? '[ ENCRYPTED ]' : '[ INSECURE ]' }`);

```

`.env`:
```
el_email=hello@example.com
el_url__frontend=/

COOKIE_SECRET=ultraTopSecret
el_cookies_domain=example.com
el_database_port=1600
el_database_encrypted=true # Booleans can be `true` or `false`
```

## License

MIT