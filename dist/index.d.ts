export declare const packageVersion: string;
export interface EnvOptions {
    aliases?: any;
    prefix?: string;
    logger?: {
        info: (str: any) => void;
    };
}
export declare function isDemo(): boolean;
export declare function isProduction(): boolean;
export declare function isDev(): boolean;
export declare function isTesting(): boolean;
export declare const REQUIRED_SYMBOL: unique symbol;
export declare const UNDEFINED_STRING: string;
export declare const UNDEFINED_NUMBER: number;
export declare const UNDEFINED_BOOLEAN: boolean;
export declare function required<T = string>(def?: T): T;
export declare function requiredInProduction<T = string>(devDefault: T): T;
export declare function defaultIfDev<T>(ifDev: T, ifProd?: T): T;
/**
 * A place to put default values for environment configurations.
 * Also ensures type consistency when loading from env.
 *
 * Values will be loaded from the environment in the format
 * `dmp_neo4j_host` - any existing underscores will be replaced
 * with TWO underscores. For example: `dmp_system_url__api`
 * If a value is not required and an environment configuration
 * does not exist it will be set to the default.
 *
 * Place a string, number, or boolean where you have a
 * consistent default value
 *
 * For defaults which start undefined, you can set a type to
 * using `UNDEFINED_STRING`, `UNDEFINED_BOOLEAN`, `UNDEFINED_NUMBER`
 *
 * If there is a default in development, but it is required
 * to be set in production use the function `requiredInProduction`
 *
 * To change the default value depending on development or production
 * environments use `defaultIfDev`
 *
 * To add an alias for a variable, add it to the
 * `ENVIRONMENT_ALIASES` object without the ENV_PREFIX portion
 */
export declare function environment<T extends {}>(cfg: T, options: EnvOptions): T;
export declare function processConfigValues(rootPath: string[], root: any, options: EnvOptions): void;
