"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processConfigValues = exports.environment = exports.defaultIfDev = exports.requiredInProduction = exports.required = exports.UNDEFINED_BOOLEAN = exports.UNDEFINED_NUMBER = exports.UNDEFINED_STRING = exports.REQUIRED_SYMBOL = exports.isTesting = exports.isDev = exports.isProduction = exports.isDemo = exports.packageVersion = void 0;
exports.packageVersion = process.env.npm_package_version || '0.0.0';
function isDemo() {
    return process.env.NODE_ENV === 'demo';
}
exports.isDemo = isDemo;
function isProduction() {
    return process.env.NODE_ENV === 'production';
}
exports.isProduction = isProduction;
function isDev() {
    return process.env.NODE_ENV === 'development';
}
exports.isDev = isDev;
function isTesting() {
    return process.env.NODE_ENV === 'testing';
}
exports.isTesting = isTesting;
exports.REQUIRED_SYMBOL = Symbol('REQUIRED_SYMBOL');
exports.UNDEFINED_STRING = Symbol('UNDEFINED_STRING');
exports.UNDEFINED_NUMBER = Symbol('UNDEFINED_NUMBER');
exports.UNDEFINED_BOOLEAN = Symbol('UNDEFINED_BOOLEAN');
function required(def) {
    return exports.REQUIRED_SYMBOL;
}
exports.required = required;
function requiredInProduction(devDefault) {
    if (isProduction()) {
        return exports.REQUIRED_SYMBOL;
    }
    return devDefault;
}
exports.requiredInProduction = requiredInProduction;
function defaultIfDev(ifDev, ifProd) {
    return isDev() ? ifDev : ifProd;
}
exports.defaultIfDev = defaultIfDev;
function applyEnvironmentAliases(options) {
    var logger = options.logger, aliases = options.aliases, prefix = options.prefix;
    // eslint-disable-next-line guard-for-in
    for (var oldKey in aliases) {
        var newKey = aliases[oldKey];
        var newKeyFull = prefix + '_' + newKey;
        if (!process.env[oldKey] || process.env[oldKey] === '') {
            continue;
        }
        if (process.env[newKeyFull] || process.env[newKeyFull] === '') {
            logger === null || logger === void 0 ? void 0 : logger.info("[SKIPPED] environment alias: " + oldKey + " -> " + newKeyFull + " already existed.");
            continue;
        }
        process.env[newKeyFull] = process.env[oldKey];
        logger === null || logger === void 0 ? void 0 : logger.info("[PROCESSED] environment alias: " + oldKey + " -> " + newKeyFull);
    }
}
/**
 * A place to put default values for environment configurations.
 * Also ensures type consistency when loading from env.
 *
 * Values will be loaded from the environment in the format
 * `dmp_neo4j_host` - any existing underscores will be replaced
 * with TWO underscores. For example: `dmp_system_url__api`
 * If a value is not required and an environment configuration
 * does not exist it will be set to the default.
 *
 * Place a string, number, or boolean where you have a
 * consistent default value
 *
 * For defaults which start undefined, you can set a type to
 * using `UNDEFINED_STRING`, `UNDEFINED_BOOLEAN`, `UNDEFINED_NUMBER`
 *
 * If there is a default in development, but it is required
 * to be set in production use the function `requiredInProduction`
 *
 * To change the default value depending on development or production
 * environments use `defaultIfDev`
 *
 * To add an alias for a variable, add it to the
 * `ENVIRONMENT_ALIASES` object without the ENV_PREFIX portion
 */
function environment(cfg, options) {
    options.prefix = options.prefix || 'app';
    var logger = options.logger, aliases = options.aliases, prefix = options.prefix;
    applyEnvironmentAliases(options);
    processConfigValues([], cfg, options);
    return cfg;
}
exports.environment = environment;
function processConfigValues(rootPath, root, options) {
    if (rootPath === void 0) { rootPath = []; }
    var prefix = options.prefix, logger = options.logger;
    // eslint-disable-next-line guard-for-in
    for (var key in root) {
        var envKey = options.prefix;
        for (var _i = 0, rootPath_1 = rootPath; _i < rootPath_1.length; _i++) {
            var a = rootPath_1[_i];
            envKey += "_" + a;
        }
        envKey += '_';
        // Replace all single underscores with double underscores
        var valEnvKey = key.replace(/_/g, '__');
        envKey += valEnvKey;
        var defaultValue = root[key];
        // skip if array
        if (Array.isArray(defaultValue)) {
            continue;
        }
        var type = typeof defaultValue;
        if (defaultValue === exports.UNDEFINED_STRING) {
            defaultValue = undefined;
            type = 'string';
        }
        if (defaultValue === exports.UNDEFINED_NUMBER) {
            defaultValue = undefined;
            type = 'number';
        }
        if (defaultValue === exports.UNDEFINED_BOOLEAN) {
            defaultValue = undefined;
            type = 'boolean';
        }
        var isRequired = defaultValue === exports.REQUIRED_SYMBOL;
        var environmentValue = process.env[envKey];
        if (isRequired && type !== 'object' && (!environmentValue || environmentValue === '')) {
            throw new Error("Configuration initialization error: Environment value " + envKey + " is required but is not set.");
        }
        if (type === 'object') {
            processConfigValues(rootPath.concat(valEnvKey), defaultValue, options);
            continue;
        }
        if (!environmentValue || environmentValue === '') {
            root[key] = defaultValue;
            continue;
        }
        if (type === 'number') {
            root[key] = parseFloat(environmentValue);
            if (isNaN(root[key])) {
                throw new Error("Configuration initialization error: Environment value " + envKey + " is invalid. Expected a number value.");
            }
        }
        else if (type === 'boolean') {
            try {
                root[key] = JSON.parse(environmentValue.toLowerCase());
            }
            catch (err) {
                throw new Error("Configuration initialization error: Environment value " + envKey + " is invalid. Expected a boolean value. " + err.message);
            }
        }
        else {
            root[key] = environmentValue;
        }
        logger === null || logger === void 0 ? void 0 : logger.info("[LOADED] environment variable: " + envKey);
    }
}
exports.processConfigValues = processConfigValues;
//# sourceMappingURL=index.js.map