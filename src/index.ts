
export const packageVersion =  process.env.npm_package_version || '0.0.0';

export interface EnvOptions {
	aliases?: any,
	prefix?: string,
	logger?: {
		info: (str) => void,
	},
}

export function isDemo() {
	return process.env.NODE_ENV === 'demo';
}

export function isProduction() {
	return process.env.NODE_ENV === 'production';
}

export function isDev() {
	return process.env.NODE_ENV === 'development';
}

export function isTesting() {
	return process.env.NODE_ENV === 'testing';
}

export const REQUIRED_SYMBOL = Symbol('REQUIRED_SYMBOL');
export const UNDEFINED_STRING = Symbol('UNDEFINED_STRING') as unknown as string;
export const UNDEFINED_NUMBER = Symbol('UNDEFINED_NUMBER') as unknown as number;
export const UNDEFINED_BOOLEAN = Symbol('UNDEFINED_BOOLEAN') as unknown as boolean;

export function required<T = string>(def?: T) {
	return REQUIRED_SYMBOL as unknown as T;
}

export function requiredInProduction<T = string>(devDefault: T) {
	if (isProduction()) { return REQUIRED_SYMBOL as unknown as T; }
	return devDefault;
}

export function defaultIfDev<T>(ifDev: T, ifProd?: T) {
	return isDev() ? ifDev : ifProd;
}

function applyEnvironmentAliases(options: EnvOptions) {
	const {
		logger,
		aliases,
		prefix,
	} = options;

	// eslint-disable-next-line guard-for-in
	for (const oldKey in aliases) {
		const newKey = aliases[oldKey];
		const newKeyFull = prefix + '_' + newKey;
		if (!process.env[oldKey] || process.env[oldKey] === '') { continue; }
		if (process.env[newKeyFull] || process.env[newKeyFull] === '') {
			logger?.info(`[SKIPPED] environment alias: ${ oldKey } -> ${ newKeyFull } already existed.`);
			continue;
		}
		process.env[newKeyFull] = process.env[oldKey];
		logger?.info(`[PROCESSED] environment alias: ${ oldKey } -> ${ newKeyFull }`);
	}
}

/**
 * A place to put default values for environment configurations.
 * Also ensures type consistency when loading from env.
 *
 * Values will be loaded from the environment in the format
 * `dmp_neo4j_host` - any existing underscores will be replaced
 * with TWO underscores. For example: `dmp_system_url__api`
 * If a value is not required and an environment configuration
 * does not exist it will be set to the default.
 *
 * Place a string, number, or boolean where you have a
 * consistent default value
 *
 * For defaults which start undefined, you can set a type to
 * using `UNDEFINED_STRING`, `UNDEFINED_BOOLEAN`, `UNDEFINED_NUMBER`
 *
 * If there is a default in development, but it is required
 * to be set in production use the function `requiredInProduction`
 *
 * To change the default value depending on development or production
 * environments use `defaultIfDev`
 *
 * To add an alias for a variable, add it to the
 * `ENVIRONMENT_ALIASES` object without the ENV_PREFIX portion
 */
export function environment<T extends {}>(
	cfg: T,
	options: EnvOptions,
) {
	options.prefix = options.prefix || 'app';

	const {
		logger,
		aliases,
		prefix,
	} = options;

    applyEnvironmentAliases(options);
    processConfigValues([], cfg, options);

    return cfg;
}


export function processConfigValues(rootPath: string[] = [], root: any, options: EnvOptions) {
	const {
		prefix,
		logger,
	} = options;
	// eslint-disable-next-line guard-for-in
	for (const key in root) {
		let envKey = options.prefix;
		for (const a of rootPath) {
			envKey += `_${ a }`;
		}
		envKey += '_';

		// Replace all single underscores with double underscores
		const valEnvKey = key.replace(/_/g, '__');
		envKey += valEnvKey;

		let defaultValue = root[key];

		// skip if array
		if (Array.isArray(defaultValue)) {
			continue;
		}

		let type = typeof defaultValue;
		if (defaultValue === UNDEFINED_STRING) {
			defaultValue = undefined;
			type = 'string';
		}

		if (defaultValue === UNDEFINED_NUMBER) {
			defaultValue = undefined;
			type = 'number';
		}

		if (defaultValue === UNDEFINED_BOOLEAN) {
			defaultValue = undefined;
			type = 'boolean';
		}


		const isRequired = defaultValue === REQUIRED_SYMBOL;
		const environmentValue: string | number | boolean = process.env[envKey];

		if (isRequired && type !== 'object' && (!environmentValue || environmentValue === '')) {
			throw new Error(`Configuration initialization error: Environment value ${ envKey } is required but is not set.`);
		}

		if (type === 'object') {
			processConfigValues(rootPath.concat(valEnvKey), defaultValue, options);
			continue;
		}

		if (!environmentValue || environmentValue === '') {
			root[key] = defaultValue;
			continue;
		}

		if (type === 'number') {
			root[key] = parseFloat(environmentValue);
			if (isNaN(root[key])) {
				throw new Error(`Configuration initialization error: Environment value ${ envKey } is invalid. Expected a number value.`);
			}
		} else if (type === 'boolean') {
			try {
				root[key] = JSON.parse(environmentValue.toLowerCase());
			} catch (err) {
				throw new Error(`Configuration initialization error: Environment value ${ envKey } is invalid. Expected a boolean value. ${ err.message }`);
			}
		} else {
			root[key] = environmentValue;
		}

		logger?.info(`[LOADED] environment variable: ${ envKey }`);
	}
}
